<?php
/*
 * бизнес-логика приложения тут
 */


class AdminController {

    /*
     * экш отображения списка опросов
     */
    function quiz_list() {
        $quiz = new Quiz();
        $actual = $quiz->getActiveQuiz();
        $closed = $quiz->getClosedQuiz();
        $draft = $quiz->getDraftQuiz();

        $tpl = new Template();
        $tpl_vars = array(
            'actual' => $actual,
            'draft' => $draft,
            'closed' => $closed,
            'quiz_partial' => __DIR__ . '/templates/admin_quiz_list_partial.html'
        );
        $tpl->show('admin_quiz_list.html', $tpl_vars);
    }

    /*
     * редактирование опроса
     */
    function edit_quiz($request, $id=0) {

        $quiz = new Quiz($id);
        if ($_POST) {
            if (!$quiz->validate()) {
                print 'Error message';die;
            }

            if (isset($request['to_delete']) && $request['to_delete'] == 1) {
                $quiz = new Quiz($request['quiz']['id']);
                $quiz->delete();
            } else {
                $id = $quiz->updateOrCreate($request['quiz']);
                $keep_questions = array();
                $keep_answers = array();
                if ($id && isset($request['question'])) {
                    foreach ($request['question'] as $k => $item) {
                        $item['quiz_id'] = $id;
                        if (isset($item['required_answer']) &&
                            $item['required_answer'] == 'on') {
                            $item['required_answer'] = 1;
                        } else {
                            $item['required_answer'] = 0;
                        }
                        $question = new Question();
                        $question_id = $question->updateOrCreate($item);
                        $keep_questions[] = $question_id;
                        foreach ($request['answer'][$k] as $ans_k => $ans_item) {
                            $ans_item['question_id'] = $question_id;
                            $answer = new Answer();
                            $keep_answers[] = $answer->updateOrCreate($ans_item);
                        }
                        //удаляем удаленные ответы вопроса
                        $answer = new Answer();
                        $answer->deleteRows(
                            'question_id = ' . $question_id . ' and id not in (' .
                            implode(', ', $keep_answers) . ')');
                    }
                    //удаляем удаленные вопросы опроса
                    $question = new Question();
                    $question->deleteRows(
                        'quiz_id = ' . $id . ' and id not in (' .
                        implode(', ', $keep_questions) . ')');
                }
            }
            Router::redirect('/admin/quiz_list');
        }

        $tpl_vars = array(
            'quiz_statuses' => $quiz->getAvailableStatuses(),
            'quiz' => $quiz,
            'question_partial' =>
                __DIR__ . '/templates/admin_quiz_edit_question_partial.html',
            'answer_partial' =>
                __DIR__ . '/templates/admin_quiz_edit_answer_partial.html',
            'curr_question_index' => count($quiz->questions),
            'result_partial' =>
                __DIR__ . '/templates/quiz_result.html',
            'data' => $quiz->resultData($quiz->id),
            'back_url' => '/admin/quiz_list'
        );

        //разрулим разршенные действия с опросом через подходящий шаблон
        if ($quiz->status == -1 || $quiz->status == Quiz::$_QUIZ_DRAFT_STATUS) {
            $tpl_main_block = 'admin_quiz_edit.html';
        } elseif ($quiz->status == Quiz::$_QUIZ_ACTIVE_STATUS) {
            $tpl_main_block = 'admin_quiz_edit_active.html';

        } elseif ($quiz->status == Quiz::$_QUIZ_CLOSED_STATUS) {
            $tpl_main_block = 'admin_quiz_edit_active.html';
        } else {
            print 'Something went wrong!'; die;
        }

        $tpl = new Template();
        $tpl->show($tpl_main_block, $tpl_vars);
    }

    /*
     * выборка результатов опроса по критериям
     */
    function query($request, $id) {
        $quiz = new Quiz($id);
        if ($quiz->status == Quiz::$_QUIZ_DRAFT_STATUS) {
            print 'По этому опросу нельзя делать выборки!';die;
        }

        $tpl_vars = array(
            'quiz' => $quiz,
            'result_partial' =>
                __DIR__ . '/templates/quiz_result.html'
        );

        if ($_POST) {
            $conditions = array();
            $condition_str = '';
            //сформируем условие для sql-запроса,
            //а также его человеческое представление
            if (isset($request['ans'])) {
                foreach ($request['ans'] as $k => $v) {
                    $answers_ids = array_keys($v);
                    $question = new Question($k);
                    $condition_str .= $question->question . ': ';
                    $answers_arr = array();
                    foreach ($answers_ids as $answer_id) {
                        $answer = new Answer(($answer_id));
                        $answers_arr[] = '<b>"' . $answer->answer . '"</b>';
                    }
                    $condition_str .= implode(' ИЛИ ', $answers_arr) . '. ';

                    $conditions[] =
                        '(question_id=' . $k . ' AND answer_id IN(' .
                            implode(',', $answers_ids) .'))';

                }
            } else {
                $conditions[] = 1;
            }
            $condition = implode(' OR ', $conditions);

            $tpl_vars['data'] = $quiz->resultData(
                $request['quiz_id'], $condition);
            $tpl_vars['condition_str'] = $condition_str;
            $tpl_vars['back_url'] = '/admin/query/' . $quiz->id;

            $tpl_main_block = 'admin_query_result.html';
        } else {
            $tpl_main_block = 'admin_query.html';
            $tpl_vars['back_url'] = '/admin/edit_quiz/' . $quiz->id;

        }

        $tpl = new Template();
        $tpl->show($tpl_main_block, $tpl_vars);
    }
}


class QuizController {

    /*
     * показывает активный опрос, который можно пройти
     */
    function show_active($request) {

        $error = '';
        if ($_POST) {
            $quiz = new Quiz($request['quiz_id']);
            if ($quiz->validateQuizAnswers($request)) {
                $quiz_answer = new QuizAnswer();
                $quiz_answer->saveQuiz($request['quiz_id'], $request);
                Router::redirect('/quiz/result');
            } else {
                $error = 'Вопросы, помеченные звездочкой, обязательны для ответа';
            }
        }

        $quiz = new Quiz();
        $active = $quiz->getActiveQuiz();
        if ($active) {
            $active = new Quiz($active[0]['id']);
        }

        $tpl_vars = array(
            'active' => $active,
            'error' => $error,
            'answers' => $request
        );

        $tpl = new Template();
        $tpl->show('quiz_show_active.html', $tpl_vars);
    }

    /*
     * результат активного опроса
     */
    function result() {
        $quiz = new Quiz();
        $active = $quiz->getActiveQuiz();
        if ($active) {
            $active = new Quiz($active[0]['id']);
        }

        $tpl_vars = array(
            'quiz' => $active,
            'data' => $quiz->resultData($active->id),
            'back_url' => '/quiz/show_active'
        );

        $tpl = new Template();
        $tpl->show('quiz_result.html', $tpl_vars);
    }
}


class Quiz extends BaseDBModel {
    public $table = 'quiz';

    public $id = 0;
    public $name;
    public $status = -1;
    public $questions = array();

    static $_QUIZ_DRAFT_STATUS = 3;
    static $_QUIZ_CLOSED_STATUS = 2;
    static $_QUIZ_ACTIVE_STATUS = 1;

    function __construct($id = null) {
        if ($id) {
            $quiz = $this->getRow('id = ' . $id);
            //пока оставим магические методы, сделаем в лоб
            if ($quiz) {
                $this->id = $quiz['id'];
                $this->name = $quiz['name'];
                $this->status = $quiz['status'];
                $question = new Question();
                $questions = $question->getRows('quiz_id = ' . $this->id);
                foreach ($questions as $k => $v) {
                    $this->questions[] = new Question($v['id']);
                }
            }
        }
    }

    function getActiveQuiz() {
        return $this->getRows('status = ' . self::$_QUIZ_ACTIVE_STATUS);
    }

    function getClosedQuiz() {
        return $this->getRows('status = ' . self::$_QUIZ_CLOSED_STATUS);
    }

    function getDraftQuiz() {
        return $this->getRows('status = ' . self::$_QUIZ_DRAFT_STATUS);
    }

    function getAvailableStatuses() {
        $active = $this->getActiveQuiz();

        $draft_rules = array(self::$_QUIZ_DRAFT_STATUS);
        $closed_rules = array(self::$_QUIZ_CLOSED_STATUS);
        if (!$active) {
            $draft_rules[] = self::$_QUIZ_ACTIVE_STATUS;
            $closed_rules[] = self::$_QUIZ_ACTIVE_STATUS;
        }

        $statuses_rules = array(
            self::$_QUIZ_DRAFT_STATUS => $draft_rules,
            self::$_QUIZ_ACTIVE_STATUS => array(
                self::$_QUIZ_ACTIVE_STATUS,
                self::$_QUIZ_CLOSED_STATUS
            ),
            self::$_QUIZ_CLOSED_STATUS => $closed_rules,
            -1 => array(
                self::$_QUIZ_DRAFT_STATUS
            )
        );

        $statuses = $this->statusesNames();
        $available_statuses = array();

        foreach ($statuses_rules[$this->status] as $kk => $status) {
            $available_statuses[$status] = $statuses[$status];
        }

        return $available_statuses;
    }

    function statusesNames() {
        return array(
            Quiz::$_QUIZ_DRAFT_STATUS => 'Черновик',
            Quiz::$_QUIZ_ACTIVE_STATUS => 'Опубликован',
            Quiz::$_QUIZ_CLOSED_STATUS => 'Закрыт',
        );
    }

    function validate() {
        //обозначим, что не забыли про валидацию на стороне сервера
        return true;
    }

    /*
     * Валидация прохождения опроса
     */
    public function validateQuizAnswers($answers) {
        $res = true;
        foreach ($this->questions as $question) {
            if ($question->required_answer) {
                $res = isset($answers['chbx'][$question->id]) ||
                    isset($answers['group'][$question->id]);

                if (!$res) {
                    break;
                }
            }
        }
        return $res;
    }

    function resultData($quiz_id, $condition = '1') {
        //нарушим инкапсуляцию
        //напишем чистым SQL - знаю что такое группировки и JOIN тоже
        //выдумывать ORM для группировок и подзапросов тут излишне
        if ($condition) {
            $condition =
                'session IN (SELECT session FROM quiz_answer WHERE ' .
                $condition . ')';
        }
        $q = <<<Q
SELECT
	Q.id AS QID,
	A.id AS AID,
	Q.question AS question,
	A.answer AS answer,
	COUNT(T1.answer_id) AS answer_count,
	G.CC AS total_answer_count
FROM
	quiz_answer AS QAT1
LEFT JOIN
(
	SELECT
		*
	FROM
		quiz_answer
	WHERE
		{$condition}
) AS T1 ON T1.id = QAT1.id
LEFT JOIN question AS Q ON QAT1.question_id = Q.id
LEFT JOIN answer AS A ON QAT1.answer_id = A.id
LEFT JOIN
(
	SELECT
		question_id,
		-- COUNT(question_id) AS CC
		COUNT(distinct(session)) AS CC
	FROM
		quiz_answer
	WHERE
		{$condition}
	GROUP BY question_id
) AS G ON QAT1.question_id=G.question_id
WHERE
    QAT1.quiz_id = {$quiz_id}
GROUP BY Q.id, A.id

UNION
-- Чтобы попали те ответы, которые ни разу не выбрали

SELECT
    AAA.question_id AS QID,
    AAA.id AS AID,
    Q.question AS question,
    AAA.answer AS answer,
    0 AS answer_count,
    G.CC AS total_answer_count
FROM
    -- INNER JOIN
    answer as AAA,
    question as Q ,
    quiz as QQ,
    (
        SELECT
            question_id,
            COUNT(distinct(session)) AS CC
        FROM
            quiz_answer
        WHERE
            {$condition}
        GROUP BY question_id
    ) AS G
WHERE
    AAA.id NOT IN
        (SELECT answer_id from quiz_answer) AND
    AAA.question_id = Q.id AND
    QQ.id = Q.quiz_id AND
    AAA.question_id = G.question_id AND
    QQ.id = {$quiz_id}

union

select
	aa.question_id AS QID,
	aa.id AS AID,
	question.question AS question,
	aa.answer AS answer,
	0 AS answer_count,
	0 AS total_answer_count
from
	answer as aa
left join question ON question.id = aa.question_id
where
	aa.question_id not in (select question_id from quiz_answer) and
	question.quiz_id = {$quiz_id}
Q;
        //print $q;die;
        $result = $this->doQuery($q);
        $rows = array();
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $rows[] = $row;
        }

        //подготовим данные
        $result = array();
        foreach ($rows as $k => $v) {
            $answer_count = (int)$v['answer_count'];
            $total_answer_count = (int)$v['total_answer_count'];
            $result[$v['question']][$v['answer']] = array(
                $answer_count,
                $total_answer_count ? round($answer_count * 100 / $total_answer_count) : 0,
                $total_answer_count
            );
        }

        return $result;
    }
}


class Question extends BaseDBModel{
    public $table = 'question';
    public $answers = array();
    public $id = 0;
    public $question;
    public $multy_answer;
    public $quiz_id;
    public $required_answer = 0;

    function __construct($id = null) {
        if ($id) {
            $question = $this->getRow('id = ' . $id);
            //пока оставим магические методы, сделаем в лоб
            if ($question) {
                $this->id = $question['id'];
                $this->question = $question['question'];
                $this->multy_answer = $question['multy_answer'];
                $this->required_answer = $question['required_answer'];
                $this->quiz_id = $question['quiz_id'];
                $answer = new Answer();
                $answers = $answer->getRows('question_id = ' . $this->id);
                foreach ($answers as $k => $v) {
                    $this->answers[] = new Answer($v['id']);
                }
            }
        }
    }
}


class Answer extends BaseDBModel {
    public $table = 'answer';
    public $id = 0;
    public $answer;

    function __construct($id = null) {
        if ($id) {
            $answer = $this->getRow('id = ' . $id);
            if ($answer) {
                $this->id = $answer['id'];
                $this->answer = $answer['answer'];
            }
        }
    }
}


class QuizAnswer extends BaseDBModel {
    public $table = 'quiz_answer';
    public $id = 0;
    public $quiz_id = 0;
    public $question_id = 0;
    public $answer_id = 0;

    function saveQuiz($quiz_id, $answers) {
        $session_id = rand();
        if (isset($answers['chbx'])) {
            foreach ($answers['chbx'] as $question_id => $answer) {
                foreach ($answer as $answer_id => $on) {
                    $p = array(
                        'id' => 0,
                        'quiz_id' => $quiz_id,
                        'question_id' => $question_id,
                        'answer_id' => $answer_id,
                        'session' => $session_id
                    );
                    $this->updateOrCreate($p);
                }
            }
        }

        if (isset($answers['group'])) {
            foreach ($answers['group'] as $k => $answer) {
                $p = array(
                    'id' => 0,
                    'quiz_id' => $quiz_id,
                    'question_id' => $k,
                    'answer_id' => $answer,
                    'session' => $session_id
                );
                $this->updateOrCreate($p);
            }
        }
    }
}