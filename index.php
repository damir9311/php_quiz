<?php

require_once __DIR__ . '/core.php';
require_once __DIR__ . '/app.php';

$config_file = __DIR__ . '/config.ini';

//обязательно загрузим конфиг
//$config = Config::getInstance($config_file);
$config = Config::getInstance()->getConfig();
//соединяемся с БД
DBConnection::getInstance();

//сам запрос
$router = new Router();

if ($config['app']['debug']) {
    $_REQUEST['q'] = $_SERVER['REQUEST_URI'];
}

$router->doRequest($_REQUEST);