$(function() {

    $(document).on('click', '#add_question_button', function() {
        var currentQuestionIndex = getCurrentQuestionIndex(),
            questionBlockTpl = getQuestionBlockTpl(),
            questionBlock =
                questionBlockTpl.replace(/IND/g, currentQuestionIndex);

        insertQuestionBlock(questionBlock);

        return false;
    });

    $(document).on('click', '.delete_question_button', function() {
        $(this).closest('.question_block').remove();
        return false;
    });

    $(document).on('click', '.add_answer_button', function() {
        var $questionBlock = $(this).closest('.question_block'),
            $answerBlock = $questionBlock.find('.answers_block'),
            thisQuestionIndex = $questionBlock.attr('number'),
            thisAnswerIndex = parseInt($answerBlock.attr('number')),
            answerBlockTpl = getAnswerBlockTpl(),
            answerBlock = $(answerBlockTpl(thisQuestionIndex, thisAnswerIndex));

        insertAnswerBlock($questionBlock, answerBlock);
        $answerBlock.attr('number', thisAnswerIndex + 1);

        return false;
    });

    $(document).on('click', '.delete_answer_button', function() {
        $(this).closest('.answer_block').remove();
        return false;
    });

    $('#d_form').validate({
        errorPlacement: function(error, element) {
            error.appendTo(element.parent().next());
        }
    });

    $.validator.addMethod('question_count', function(value, element) {
        var r = $('#questions_block .question_block').length >=
            QUESTION_COUNT_VALIDATE;
        return r;
    }, 'Должен быть введен хотя бы ' + QUESTION_COUNT_VALIDATE + ' вопрос');

    $.validator.addMethod('per_question_answer_count', function(value, element) {
        var r = true
        $('#questions_block .question_block').each(function(index, el) {
            if ($(el).find('.answer_block').length <
                    PER_QUESTION_ANSWER_COUNT_VALIDATE) {
                r = false;
            }
        });

        return r;
    }, 'Для каждого введенного вопроса должно быть введено не менее ' +
            PER_QUESTION_ANSWER_COUNT_VALIDATE +
            ' ответов'
    );

    $.validator.addMethod('required_answer_count', function(value, element) {
        var r = $('#questions_block input[type=checkbox]:checked').length >=
            REQUIRED_QUESTION_ANSWER_COUNT_VALIDATE;
        return r;
    }, 'Хотя бы ' + REQUIRED_QUESTION_ANSWER_COUNT_VALIDATE +
            ' из введенных вопросов должен быть обязательным для ответа');

});

function getCurrentQuestionIndex() {
    return currQuestionIndex;
}

function getQuestionBlockTpl() {
    return $('#question_block_tpl').html();
}

function insertQuestionBlock(questionBlock) {
    $('#questions_block').append(questionBlock);
    currQuestionIndex += 1;
}

function getAnswerBlockTpl() {
    return jQuery.validator.format($.trim($('#answer_block_tpl').val()));
}

function insertAnswerBlock(question_block, answerBlock) {
    question_block.find('.answers_block').append(answerBlock);
}
