<?php

//путь до конфига приложения
define('DEFAULT_CONFIG', __DIR__ . '/config.ini');

//параметр по умолчанию строки запроса (у меня задан в nginx)
define('DEFAULT_REQUEST_URI_PARAM', 'q');


/*
 * Синглтон настроек приложения
 */
class Config {
    protected static $_instance;
    private $config_file;
    protected $config;

    private function __construct($config_file) {
        $this->config_file = $config_file;
        $this->config = parse_ini_file($this->config_file, true);
    }
    private function __clone(){}

    public static function getInstance($config_file=DEFAULT_CONFIG) {
        if (null === self::$_instance) {
            self::$_instance = new self($config_file);
        }
        return self::$_instance;
    }

    function getConfig() {
        return $this->config;
    }
}


/*
 * Синглтон поддерживающий соединение с БД.
 */
class DBConnection {

    protected static $_instance;

    //настройки соединения с БД
    private $hostname;
    private $username;
    private $password;
    private $database;

    //private - потому что синглтон
    private function __construct() {
        $this->config = Config::getInstance()->getConfig();
        $this->readParams();
        $this->connectDB();
    }
    private function __clone(){}

    /*
     * Так получаем объект синглтона, по факту это и есть соединение с БД
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    private function readParams() {
        //TODO: конечно здесь нужна проверка существования ключей
        $this->hostname = $this->config['database']['hostname'];
        $this->username = $this->config['database']['username'];
        $this->password = $this->config['database']['password'];
        $this->database = $this->config['database']['name'];
    }

    private function connectDB() {
        //TODO: выкидывать исключение в случае неполадок
        mysql_connect($this->hostname, $this->username, $this->password);
        mysql_select_db($this->database);
    }
}


/*
 * Базовый класс работы с таблицами БД
*/
class BaseDBModel {
    //не будем заморачиваться композицией и интерфейсами - пока тут все просто

    public $table;

	function getRows($condition = null) {
        //все будет очень просто
        $query =
            'select * from ' . $this->table . ' where ' .
                ( $condition ? $condition : 1);
        $result = $this->doQuery($query);

        $rows = array();
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getRow($condition = null) {
        $rows = $this->getRows($condition);
        $res = null;
        if ($rows) {
            $res = $rows[0];
        }
        return $res;
    }

    function doQuery($query) {
        return mysql_query($query);
    }

    function updateOrCreate($values) {
        $id = $values['id'];
        unset($values['id']);

        if (!$this->getRows('id = '. $id)) {

            unset($values['id']);
            $keys = array_keys($values);
            $insert_key_values = array_values($values);

            $make_key = function($k) {
                return '`' . $k . '`';
            };
            $make_value = function($v) {
                return '\'' . $v . '\'';
            };

            $query = 'insert into `' .
                $this->table . '` (' .
                implode(', ', array_map($make_key, $keys)) .
                ') values (' .
                implode(', ', array_map($make_value, $insert_key_values)) .
                ')';
            $this->doQuery($query);
            //что-то тут не так?
            return mysql_insert_id();
        } else {
            $update_query_str = array();
            foreach ($values as $k => $v) {
                $update_query_str[] = '`' . $k . '` = \'' . $v . '\'';
            }
            $query = 'update `' .
                $this->table .
                '` set ' . implode(', ', $update_query_str) .
                ' where id = ' . $id;
            $this->doQuery($query);
            return $id;
        }
    }

    function deleteRows($condition='1') {
        //опасная операция!!!
        $query = 'delete from `' . $this->table . '` where ' . $condition;
        $this->doQuery($query);
    }

    function delete() {
        $this->deleteRows('id = ' . $this->id);
    }
}

/*
 * Класс разруливающий запросы по экшенам
 */
class Router {

    private $config;
    private $request_uri_param;

    function __construct() {
        $this->config = Config::getInstance()->getConfig();

        if (isset($this->config['request']) &&
            isset($this->config['request']['request_uri_param']))
            $this->request_uri_param =
                $this->config['request']['request_uri_param'];
        else
            $this->request_uri_param = DEFAULT_REQUEST_URI_PARAM;
    }

    function doRequest($request) {

        // $controller/$action[/$param1/.../$paramN]
        //строка запроса
        $q = $request[$this->request_uri_param];
        if ('/' == $q) {
            $q = $this->config['request']['default_uri'];
        }

        //разбор адреса
        //TODO: ВАЖНО! Т.к. это учебный пример, то здесь не рассматриваются
        //многие случаи обработки запроса, предполагается, что вводим корректные
        //адреса и параметры если надо. Также в приложении нет $_GET параметров,
        //если надо, то через слеши третьей и следующими частями адреса
        $a_arr = explode('/', $q);

        //первый элемент пустое значение
        array_shift($a_arr);

        $controller_class = ucfirst(strtolower(array_shift($a_arr))) .
            'Controller';
        $action = strtolower(array_shift($a_arr));

        //создаем объект вызываемого контроллера
        if (class_exists($controller_class)) {
            $controller_object = new $controller_class();
        } else {
            print $controller_class . ' controller not found'; die;
        }

        //для него экшн, для простоты не будем выдумывать свой request
        if (method_exists($controller_object, $action)) {
            //вызываем так, для того, чтобы передать произвольное
            //количество аргуметов в экшены, первым и единственно
            //обязательным параметром является $request
            array_unshift($a_arr, $request);
            call_user_func_array(array($controller_object, $action), $a_arr);
        } else {
            print 'Action not found'; die;
        }
    }

    static function redirect($url) {
        //самый простой способ
        header('Location: ' . $url);
        die();
    }
}


/*
 * Псевдошаблонизатор
 */
class Template {

    private $config;

    public $main_tpl;

    function __construct() {
        $this->config = Config::getInstance()->getConfig();

        $this->main_tpl = $this->config['template']['dir'] . '/' .
            $this->config['template']['main_template'];
    }

    function show($tpl_path, $tpl_vars = array()) {
        $block_tpl = $this->config['template']['dir'] . '/' . $tpl_path;
        $block = __DIR__ . '/' . $block_tpl;
        require_once __DIR__ . '/' . $this->main_tpl;
    }
}